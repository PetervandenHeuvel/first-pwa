//import { calculate } from "./js/modules/calculate.js";

import { calculate } from "./modules/calculate.js";

window.onload = () => {
  "use strict";
  // if ("serviceWorker" in navigator) {
  //   navigator.serviceWorker.register("./sw.js");
  // }

  const input = document.querySelector('input[name="datum"]');

  input.addEventListener("change", () => {
    let currentInput = input.value;
    console.log("currentInput is" + currentInput);

    if (currentInput !== "null") {
      $(".form").hide();
      $(".counter").show();
      doStuff();
    } else {
      $(".warning")
        .text("Er ging iets mis!")
        .show();
    }
  });

  function doStuff() {
    // Initialize a new birthday instance
    console.log("jow");

    calculate();
  }
};
